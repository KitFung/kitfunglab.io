---
title: UVA 11800 Determine the Shape
date: 2016-05-31 22:18:32
tags:
  - uva
  - geometry
---

先把點逆時指排一下, 用 monotone 就可以了. 然後按照它的條件一個一個去判斷.
我用了 dot product 去計算角度
![Dot product](https://upload.wikimedia.org/math/3/e/5/3e530da12e51ca0056ed3ef061b79312.png)

slope 去決定是否平衡.

<!-- more -->

{% gist 4323e6df4c84ed2df5af69625289ea22 %}