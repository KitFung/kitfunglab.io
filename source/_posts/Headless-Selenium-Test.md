---
title: Selenium Test in Headless Environment
date: 2016-05-22 22:36:04
tags:
	- selenium
	- automatic test
---
Selenium 是一個主流的 automatic web test 工具, 他可以模擬使用者和網頁的互動. 你可以利用他為你的網頁提供自動的測試. 當你想更進一步自動化或時, 你就可能會想把測試放上 CI 伺服器, 可是 CI 伺服器是沒有圖形界面的, 運行一般的瀏覽器時, 會出現錯誤. 

當然你可以改用 headless browser, 例如 phantomjs. 使用 headless browser 有時侯會導致更多的問題. 而且它不能代表到一般的使用者 (一般人可不會用 headless browser 上網) . 所以我們要用以下的工具了 - Xvfb.

Xvfb 可以幫助你處理圖像輸出, 即使沒有圖形界面也可以運行圖形程式.

我會介紹兩種方法. 第一種方法是自行設定整個環境. 第二個方法就是使用 docker + docker-selenium. 方法二會比較不容易出錯.

<!-- more -->

##

我的環境是 **Ubuntu server** 

## 方法一: 自行設定環境

##### 安裝 Xvfb
```bash
sudo apt-get install xvfb
```

##### 設定 testing project
設定你的 Selenium project, 確定你可以在 bash 中呼叫指令運行測試. 我建議使用 maven 去建置 selenium. 他可以幫你輕鬆地下載依賴及輸出測試結果.

##### 下載瀏覽器
這是我使用的, 當然你也可以直接 curl 你想要的瀏覽器的位元檔

```bash
sudo npm install protractor -g
sudo webdriver-manager update
sudo ln /usr/local/lib/node_modules/protractor/selenium/chromedriver_2.21 /usr/bin/chromedriver
```

##### 設定xvfb
把以下一句加入`.bashrc` 或 `.bash_profile`
````bash
export DISPLAY=:99
````

##### 然後就可以運行測試了~

```bash
# 先啟動xvfb :99是螢幕號碼 1024x768x16是螢幕大小及pixel depth
Xvfb :99 -screen 0 1024x768x16　&
# 然後運行測試
mvn test
# 如果要停止xvfb
killall xvfb
```
最後就可以把 test report (假設你有設定report plugin) 拉回本機或指定 CI 去讀取.


## 方法二: docker + docker-selenium

docker-selenium: https://github.com/elgalu/docker-selenium

這比上一個方法要做多一點. 

##### 1. 先安裝 docker 及 docker-compose
https://docs.docker.com/engine/installation/linux/ubuntulinux/

##### 2. 把建立 webdriver 的方法改成下面
因為 selenium 及 browser 會在另一個 container 運行, 如果不設定`LocalFileDetector`, 在upload file 時他會在 browser 的 container 找檔案, 而那是錯誤的. `CHROME_PORT_4444_TCP_ADDR` 是 docker 幫你加入的環境變數, 之後會介紹如何設定.

```java
String hostAddr = System.getenv("CHROME_PORT_4444_TCP_ADDR");
	   driver = new RemoteWebDriver(new URL("http://" + hostAddr + ":4444/wd/hub"),  capabilitiesFactory(properties.getProperty("TARGET_BROWSER")));
	   driver.setFileDetector(new LocalFileDetector());
```

##### 3.  把 test project 打包成 docker image
 把test project 打包成 docker image, 在建立 container 最後時運行測試.
這要先學習一下docker 的原理及如何寫 Dockerfile

##### 4. 建立 docker-compose.yml
他會把`CHROME_PORT_4444_TCP_ADDR`這個變數加入test project 的 container 中.
```yaml
chrome:
  image: selenium/standalone-chrome:2.53.0
  ports:
    - "4444:4444"
  environment:
    - SCREEN_WIDTH=1280
    - SCREEN_HEIGHT=1024
    - SCREEN_DEPTH=16
test-project:
  image: youraccount/test-project-image
  links:
    - chrome
```

##### ４. 運行了~

```bash
docker-compose up
```
要拿回 test report 的可以運行, 當然路徑要自行改動
```bash
docker cp $(docker ps -q test-project):/file/path/within/container /host/path/target
```
最後要把兩個container 都刪去. 不用擔心, 再建立一個 container 都只是幾秒的時間.　如果怕 test-project 下載依賴需時, 你可以把放依賴的資料夾 link 到 host

```bash
docker-compose rm
```