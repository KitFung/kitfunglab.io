---
title: UVA 11623 Tic Tac Toe
date: 2016-05-31 22:10:49
tags:
	- uva
---

這題基本思路不難, 但實作很麻煩. 要把所有 Error case 找出來. 我第一次的答案 TLE 了. 花了太多時間, 不想再拖下去, 找了一下其他答案, 參考一下, 自己再寫.

注意, 這答案沒有考慮當一直線長度超過 m*2 - 1　的情況, 不過 uva 似乎沒有這個 test case, 所以是 AC

<!-- more -->

{% gist 368ef6a1d6b57a150a3efc3ce51a3223 %}