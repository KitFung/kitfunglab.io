---
title: UVA 10192 Vocation
date: 2016-05-31 22:23:45
tags:
	- uva
	- dp
---

求 longest common substring, 可以用 dp 或其他字串的算法.

<!-- more -->

{% gist d68bea2da3285bc3862775f2148ee13b %}
