---
title: Apollo Cyber Study P13 Blocker
date: 2019-01-14 17:08:18
tags:
  - Apollo
---

因為Blocker中其實沒有甚麼特別值得說的。
就大概說一下它是做甚麼的吧

在cyber中，從數據傳輸，分發有關的主要為`transport`,`data visitor`跟`blocker`三個模塊
- `transport`負責最底層的數據傳輸 工作，也提供了跨進程數據傳輸的能力。而寫數據到channel都是直接由`transport`去處理的
- `data visitor`則根據由`transport`模塊得到的數據去提供一個讀數據的接口，而這接口在cyber內部被大量使用，比如`Component`的`Proc`接的數據就是從`data visitor`來的
- 而`blocker`則是把從`Node::Reader`那邊得到數據（`Node::Reader`的數據是由`data visitor`得到的　），然後放到自己的message queue中，再提供一個接口給`Node::Reader`用。主要就幫`Reader`管理數據相關的邏輯。而`Node::Reader`的接口也是開發者們會用到的（而abi跟以前adapter提供的很像　）。

cyber外部模塊的開發者只會用到`Node::Reader`,而不是`data visitor`,`transport`跟`Blocker`

{% asset_img all.png "all" %}
