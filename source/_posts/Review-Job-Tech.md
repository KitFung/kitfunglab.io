---
title: Review Job Skill
date: 2018-02-12 21:54:59
tags:
  - review
  - job
---

想說在過年前把工作上學到的事情簡單記錄一下
一直都在忙工作,沒有時間做題,慚愧.........

### 工作簡述

在 Jingchi.ai 無人車開發團隊當工程師,　主要負責地圖建立及相關事項.　也幹很多其他有的無的.
也會做一下　定位　相關的事情. 算偏工程吧.

###　接觸到的領域

####  1. 機械人相關

- 機械有甚麼不同模塊,　如何交換
- 機械人如何動起來
- 基本概念,知識
- 如何同步及做用不同傳感器之數據
- calibration

####  2. SLAM 相關

- 建地圖要用到的知識,　數學基本
- 常用技術
- kalman filter, pose graph optimization
- mapping algorithms

####  3. Computer Vision 相關

- 不知道如何說起

####  4. Compile 相關

- 如何compile及解決各種由此引出的問題

####  5. 工具開發,　優化相關

- 就 c++17, 各種 memory optimize, multi-thread, 跟一些善用算法的加速

####  6. 前後端及deploy相關

- 簡單事情, 沒有人做就我來做

####  7. Machine learning

- 就看別人代碼,　跟聽他們分享......　基本上我沒有用到

####  8. 工程化相關事項

### 反思

- 不是computer vision出身, machine learning 也只是略懂
- 買了一些相關的書,　還在慢慢看
- 還是不懂別人怎找到不同的技術paper
- 其實概念上還明白,　但數學細節上...
- 基本知識要更深入, 要更了解這領域發展未來
