---
title: Apollo Cyber Study - Cyber Launch Python Script
date: 2019-01-07 10:25:33
tags:
  - Apollo
---

`cyber` 的所謂dag很大程度是為了`perception`而存在的吧

Since its python code is simple, don't commnet on it source code.
Just describe it overall concept

`cyber_launch` is the launcher of the apollo modules.
`cyber_launch` is actually just a python script, same as `cyber_launch.py`

Apollo first compile the main module as a shared library.
The wrap the path to the shared object and it required parameter to a xml file

Then use the `cyber_launch` to parse the xml file,
and `start`/`stop` the related modules.


{% asset_img flow.png "Basic FLow" %}


### How it handle 'Start'

Parse Launch file. Then using the `mainboard` to launch if it is library. Otherwiser, launch it binary if it is Binary.

### How it handle 'Stop'

It is a simple `pkill`, it will `pkill` while using the launch file name as the regex
(It is actually killing the `cyber_launch` script with the target lanuch file as argument).

If haven't provide the launch file, it will try to kill `cyber_launch` directly.
Since the `cyber_launch` have registered the atexit callback. It will also call stop to all its
child treads. Moreover, since the modules is launch as a Daemon thread of the cyber_launch script.
We can ensure that the corresponding modules is shutdown while the killing the `cyber_launch` script
