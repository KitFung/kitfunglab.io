---
title: UVA 10330 Power Transmission
date: 2016-05-25 21:52:40
tags:
  - uva
  - network flow
---
因為某些原因, 這題做了兩次. 所以也用了兩個 network flow algorithm. 分別其實不大, Dinic 會比 ek 快一點.

這題就基本的 maximun network flow, 故不解釋.

<!-- more -->

#### Edmonds Karp algorithm 的解法
{% codeblock uva10330_ek.cpp lang:cpp%}
#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>
using namespace std;

const int MAX_PT = 230;
const int sINFINITY = 1e10;
const int START_POINT = 0;
const int END_POINT = 101;

typedef int Graph[MAX_PT][MAX_PT];


int max_flow;
int npoints, nlinks;
Graph Capacity, Flow, Remain;
bool visited[MAX_PT];
int parent[MAX_PT];
int lflow[MAX_PT];


// find argument path, Edmonds-Karp
int bfs(int s, int t) {
    memset(visited, false, sizeof(visited));
    queue<int> Q;

    parent[s] = s;
    lflow[s] = sINFINITY;
    visited[s] = true;
    Q.push(s);

    while(!Q.empty()) {
        int k = Q.front(); Q.pop();
        for(int j = 0; j < MAX_PT; ++j) {
            if(!visited[j] && Remain[k][j] > 0) {
                visited[j] = true;
                parent[j] = k;
                lflow[j] = min(lflow[k], Remain[k][j]);
                Q.push(j);
                if(j == t) return lflow[j];
            }
        }
    }
    return 0;
}

void edmonds(int s, int t) {
    memset(Flow, 0, sizeof(Flow));
    memcpy(Remain, Capacity, sizeof(Capacity));
    int min_f;
    for(max_flow = 0; min_f = bfs(s, t); max_flow += min_f) {
        for(int i = parent[t], j = t; i != j; j = i, i = parent[i]) {
            Flow[i][j] = Flow[i][j] + min_f;
            Flow[j][i] = -Flow[i][j];
            Remain[i][j] = Capacity[i][j] - Flow[i][j];
            Remain[j][i] = Capacity[j][i] - Flow[j][i];
        }
    }
}



int main() {
    // freopen("input.txt", "r", stdin);
    int from, to, cap;
    int n_sp, n_ep;
    int sp, ep;

    while(scanf("%d", &npoints) != EOF) {
        memset(Capacity, 0, sizeof(Capacity));

        for(int i = 1;i <= npoints; ++i) {
            scanf("%d", &cap);
            Capacity[i*2][i*2+1] = cap;
        }
        scanf("%d", &nlinks);
        for(int i = 0; i < nlinks; ++i) {
            scanf("%d%d%d", &from, &to, &cap);
            Capacity[from * 2 + 1][to * 2] = cap;
        }
        scanf("%d%d", &n_sp, &n_ep);
        for(int i = 0; i < n_sp; ++i) {
            scanf("%d", &sp);
            Capacity[START_POINT * 2][sp * 2] = sINFINITY;
        }
        for(int i = 0; i < n_ep; ++i) {
            scanf("%d", &ep);
            Capacity[ep * 2 + 1][END_POINT * 2] = sINFINITY;
        }

        edmonds(START_POINT*2, END_POINT*2);

        printf("%d\n", max_flow);
    }
    return 0;
}   
{% endcodeblock %}


#### Dinic Algorithm 的解法:
{% codeblock uva10330_dinic.cpp lang:cpp%}
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <queue>
using namespace std;

const int INFI = 1 << 30;
const int MAXN = 110;
const int START_NODE = 0;
const int END_NODE = 9999;

typedef struct edge {
    int to, flow, cap, next;
} Edge;

Edge E[10100];
int adj[10100];
int allow_map[10100];
int eidx;

void add_edge(int from, int to, int flow) {
    E[eidx].to = to;
    E[eidx].cap = flow;
    E[eidx].flow = 0;
    E[eidx].next = adj[from];
    adj[from] = eidx++;
    E[eidx].to = from;
    E[eidx].cap = 0;
    E[eidx].flow = 0;
    E[eidx].next = adj[to];
    adj[to] = eidx++; 
}

bool bfs(int s, int t) {
    memset(allow_map, -1, sizeof(allow_map));
    queue<int> Q;
    Q.push(s);
    allow_map[s] = 0;

    while(!Q.empty()) {
        int cur = Q.front(); Q.pop();

        for(int i = adj[cur]; i != -1; i = E[i].next) {
            Edge &e = E[i];
            if(allow_map[e.to] < 0 && e.cap > e.flow) {
                allow_map[e.to] = allow_map[cur] + 1;
                Q.push(e.to);
                if(e.to == t) {
                    return true;
                }
            }
        }
    }

    return false;
}

int dfs(int cur, int end_pt, int minflow) {
    if(cur == end_pt || minflow == 0) return minflow;

    int flow = 0, minf;
    for(int i = adj[cur]; i != -1; i = E[i].next) {
        Edge &e = E[i];

        if(allow_map[cur] + 1 == allow_map[e.to] && (minf = dfs(e.to, end_pt, min(minflow, e.cap - e.flow))) > 0) {
            flow += minf;
            e.flow += minf;
            E[i ^ 1].flow -= minf;
            minflow -= minf;
            if(minflow == 0) break;
        }
    }

    return flow;
}

int dinic(int s, int t) {
    int max_flow = 0;
    while(bfs(s, t)) {
        max_flow += dfs(s, t, INFI);
    }
    
    return max_flow;
}

void read_input(int n) {
    memset(adj, -1, sizeof(adj));
    eidx = 0;
    int nline, b, d, t;
    int from, to, flow;
    for(int i = 1; i <= n; ++i) {
        scanf("%d", &t);
        add_edge(i, i+MAXN, t);
    }

    scanf("%d", &nline);
    for(int i = 0; i < nline; ++i) {
        scanf("%d%d%d", &from, &to, &flow);
        add_edge(from+MAXN, to, flow);
    }

    scanf("%d%d", &b, &d);
    for(int i = 0; i < b; ++i) {
        scanf("%d", &t);
        add_edge(START_NODE, t, INFI);
    }
    for(int i = 0; i < d; ++i) {
        scanf("%d", &t);
        add_edge(t+MAXN, END_NODE, INFI);
    }
}


int main() {
    int n;
    while(scanf("%d", &n) != EOF) {
        read_input(n);

        int maxflow = dinic(START_NODE, END_NODE);
        printf("%d\n", maxflow);
    }

    return 0;
}
{% endcodeblock %}