---
title: Microsoft ImagineHack 2017
date: 2017-01-23 22:42:27
tags: fun
---

一直都想知道 hackathon  是甚麼一回事, 就參加一下這個, 勝負不太重要. 時長兩天, 中間要通宵工作

題目滿空泛的, 只要有關 social innovation, fintech 或 health 的作品就可以.

另外一定要用到 Microsoft Azure  的服務

### 簡單介紹一下

成品幾乎都是手機 App, 只有幾組做了chat bot.

差不多所有組, 技術上沒有甚麼突出之處. 不過時間也不多, 做簡單有噱頭的也很正常.

我那組真正在coding的只有我, 我很少做手機程式(我原本以為我可以找別人做UI, 我只做logic), 所以做了web app

題目是書本租借, 送書等. 用 flask 做網頁, 用 docker deploy

https://github.com/Bookeverflow/Bookeverflow



說實話, 沒有用到多少azure, 而UI上也滿差的(如果沒有專門設計的人, 我做的UI都很醜....), 功能也不夠突出.

大部份時間都用在 UI 及 database 上, 因為頁面及model有點多, 因為這偏向像一個平台, 沒有甚麼特出的功能.

反觀其他組都專著在核心功能上, 而最終產品都只有一,兩個頁面. 出來的效果都不錯.



### 反思

- hackathon 還是手機程式佔優, 一來手機本身的UI就不錯, 二來容易當場展示, 比較吸引注意力
- 技術不太重要, 題目及展示比較重要
- 要專注一個功能, 不要做平台類的產品
- 最好先找相熟的人參加, 包括一個設計人員, 商科的人反而不太重要
- 一定要讓評判清楚看見成品