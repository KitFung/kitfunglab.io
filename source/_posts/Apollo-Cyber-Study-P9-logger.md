---
title: Apollo Cyber Study. P9 logger
date: 2019-01-10 14:20:23
tags:
  - Apollo
---

`// Study: ` 是我的筆記

{% asset_img logger.png "Basic FLow" %}

`cyber/logger/logger_util`

```cpp
inline int64_t CycleClock_Now() {
  struct timeval tv;
  gettimeofday(&tv, nullptr);
  return static_cast<int64_t>(tv.tv_sec) * 1000000 + tv.tv_usec;
}

static inline void GetHostName(std::string* hostname) {
  struct utsname buf;
  // Study: http://man7.org/linux/man-pages/man2/uname.2.html
  //        a syscall for uname
  if (0 != uname(&buf)) {
    *buf.nodename = '\0';
  }
  *hostname = buf.nodename;
}

const std::vector<std::string>& GetLoggingDirectories();

int32_t GetMainThreadPid();

bool PidHasChanged();

inline int32_t MaxLogSize() {
  return (FLAGS_max_log_size > 0 ? FLAGS_max_log_size : 1);
}

// Study: If the log_messaeg already have [xx], use xx as modules_name
//        Otherwise, use the ProcessGroup (set in conf) as module name
inline void FindModuleName(std::string* log_message, std::string* module_name) {
  auto lpos = log_message->find('[');
  if (lpos != std::string::npos) {
    auto rpos = log_message->find(']', lpos);
    if (rpos != std::string::npos) {
      module_name->assign(*log_message, lpos + 1, rpos - lpos - 1);
      auto cut_length = rpos - lpos + 1;
      log_message->erase(lpos, cut_length);
    }
  }
  if (module_name->empty()) {
    *module_name = common::GlobalData::Instance()->ProcessGroup();
  }
}


namespace {
  // Study: syscall
  static int32_t g_main_thread_pid = getpid();
}

int32_t GetMainThreadPid() { return g_main_thread_pid; }

bool PidHasChanged() {
  int32_t pid = getpid();
  if (g_main_thread_pid == pid) {
    return false;
  }
  g_main_thread_pid = pid;
  return true;
}

const std::vector<std::string>& GetLoggingDirectories() {
  static std::vector<std::string> logging_directories_list;
  if (logging_directories_list.empty()) {
    if (!FLAGS_log_dir.empty()) {
      logging_directories_list.emplace_back(FLAGS_log_dir.c_str());
    } else {
      logging_directories_list.emplace_back("./");
    }
  }
  return logging_directories_list;
}
```

`cyber/logger/log_file_object`

Take over the low level file-interactive function. Can be seem as the `real` logger

`cyber/logger/async_logger`

Just watch its describe is ok

```cpp
// Wrapper for a glog Logger which asynchronously writes log messages.
// This class starts a new thread responsible for forwarding the messages
// to the logger, and performs double buffering. Writers append to the
// current buffer and then wake up the logger thread. The logger swaps in
// a new buffer and writes any accumulated messages to the wrapped
// Logger.
//
// This double-buffering design dramatically improves performance, especially
// for logging messages which require flushing the underlying file (i.e WARNING
// and above for default). The flush can take a couple of milliseconds, and in
// some cases can even block for hundreds of milliseconds or more. With the
// double-buffered approach, threads can proceed with useful work while the IO
// thread blocks.
//
// The semantics provided by this wrapper are slightly weaker than the default
// glog semantics. By default, glog will immediately (synchronously) flush
// WARNING
// and above to the underlying file, whereas here we are deferring that flush to
// a separate thread. This means that a crash just after a 'LOG_WARN' would
// may be missing the message in the logs, but the perf benefit is probably
// worth it. We do take care that a glog FATAL message flushes all buffered log
// messages before exiting.
//
// NOTE: the logger limits the total amount of buffer space, so if the
// underlying
// log blocks for too long, eventually the threads generating the log messages
// will block as well. This prevents runaway memory usage.
```

This logger is registered using this,
then can use the regular glog operation to use it

```cpp
google::base::SetLogger(FLAGS_minloglevel, async_logger);
```

`cyber/logger/logger`

Similiar to regular glog, but use `LogFileObject` to maintain the underlying write and naming, etc
Don't have the double buffering, etc.

