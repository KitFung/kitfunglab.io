---
title:  Apollo Cyber Study. (cyber/event)
date: 2019-01-07 17:46:48
tags:
  - Apollo
---

比起`cyber/base`中的各種實現。　`cyber/event`就好像沒有甚麼好說了

不過也是要記錄一下

基本兩個class - `perf_event`, `perf_event_cache`

顧明思義，這東西就是用來記錄各個event的

`perf_event`: event的定義及序列化格式。包括了`SCHED_EVENT, TRANS_EVENT, TRY_FETCH_EVENT`,
第三個沒有真的用上。

- `SCHED_EVENT`: 由scheduler產生的event, 記錄被schedule的task的id, state, proc, etc.
- `TRANS_EVENT`: transport, 在`cyber/transport`中生出transport的msg id, seq, etc

`perf_event_cache`: 這就是管理perf event的地方，　cache提供一個singleton給其他模塊去add event. 而cache內部先把event都放到`BoundedQueue`.　讓io thread不停去累積event message, 當event夠一定程度就做一次寫。　可以減少大量小型io的浪費.
