---
title: 'UVA 998 Many Paths, One Direction'
date: 2016-05-31 20:18:55
tags:
	- uva
	- dfs
---

看到問題就覺得是 dfs, 交了去 uva  之後一直都就是 `in the judging queue` 的狀態. 所以用空間換取時間, 把於每個Event的結果儲起來, 省去重疊的計算.

交完之後就 AC ,但前一次的 submission 還是`in the judging queue` ....

<!-- more -->

{% gist fbd9bac8905e6ced9cf4d1b40f393b6d %}
