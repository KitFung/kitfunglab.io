---
title: ICPC 2016 World Final Problem K String Theory
date: 2016-07-04 01:01:04
tags: acm
---

當遇到 `3 2 1 1 2 4 2 1 1 2 3` 這樣的情況下
只要把它當成 `3 2 1 1 1 1 1 1 1 1 1 1 1 1 2 3`
就可以發現我們只要從左右都試一次(z , z-1, z-2, ..., 1)就知道z是不是他的層數
我們由最大可能開始向下, 即是 `min(v[0], v[n-1])` , 一直試到 1.
如果 sum of a 是單數就一定沒有答案, 因為quote一定是雙數的

重點是在想解法時, 應先用多一點時間去簡化題目
不然我們只想到要證明 `3 2 1 1 2 4 2 1 1 2 3` 的正確性
而想不到把 `3 2 1 1 2 4 2 1 1 2 3` 簡化成 `3 2 1 1 1 1 1 1 1 1 1 1 1 1 2 3`
問題就會解不開

<!-- more -->

{% codeblock icpc2016K.cpp lang:cpp %}
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;

int n, a;
int sum, limit;
int v[110];
int v2[110];

bool validate(int t) {
  memcpy (&v2, &v, sizeof(v));

  int t2 = t;
  for(int i = 0; i < n; ++i) {
    while(v2[i] > 0) {
      if(v2[i]-t2 < 0) return false;
      v2[i] -= t2--;
      if(t2 == 0) break;
    }
    if(t2 == 0) break;
  }

  if(t2 != 0) return false;
  t2 = t;
  for(int i = n - 1; i >= 0; --i) {
    while(v2[i] > 0) {
      if(v2[i]-t2 < 0) return false;
      v2[i] -= t2--;
      if(t2 == 0) break;
    }
    if(t2 == 0) break;
  }
  return t2 == 0;
}

int main() {

  while(scanf("%d", &n) != EOF) {
    sum = 0;

    for(int i = 0; i < n; ++i) {
      scanf("%d", &v[i]);
      sum += v[i];
    }

    limit = min(v[0], v[n - 1]);

    if(sum%2 == 1) {
      printf("no quotation\n");
    } else {
      bool found = false;
      for(int i = limit; i > 0; --i) {
        if(validate(i)) {
          found = true;
          printf("%d\n", i);
          break;
        }
      }
      if(!found) printf("no quotation\n");
    }
  }
  return 0;
}
{% endcodeblock %}
